# README #

### What is this repository for? ###

* Vamos a usar este repositorio para registrar la forma en la que trabajamos en TUXD
* Version 0.1
* URL del wiki: [https://bitbucket.org/theuxdepartment/tuxdkb/wiki/Home](https://bitbucket.org/theuxdepartment/tuxdkb/wiki/Home)

### Documentación ###

* [Documentación sobre cómo usar el wiki](https://confluence.atlassian.com/bitbucket/use-a-wiki-221449748.html)
* [Documentación sobre markdown](https://bitbucket.org/tutorials/markdowndemo)

### Guías para contribuir ###

* Escribir con claridad.
* Ir al punto.
* Las notas tienen que servir para guiarnos a nosotros, a nuevos miembros del equipo y a clientes

### Soporte ###

Admin del repo: Juan Pablo Manson [jp@theuxdepartment.com](mailto:jp@theuxdepartment.com)